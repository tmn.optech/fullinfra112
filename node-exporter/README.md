## Исполнение роли

Роль предназначена для установки node_exporter c auth basic.

Роль поддерживает установку, конфигурацию auth и tls к node_exporter endpoints

Поддерживает OS: deb base, rpm base

### example playbook
```
- hosts: all
  remote_user: panarinis
  gather_facts: yes
  become: true
  roles:
    - node_exporter
```

### Определяем переменную path к ролям или default /etc/ansible/roles/node-exporter

```
ansible-playbook -i inventory playbook.yml -vvv
```
### Часто используемые переменные 
```
node_exporter_version: 1.0.1
node_exporter_binary_local_dir: ""
node_exporter_web_listen_address: "0.0.0.0:9100"
node_exporter_web_telemetry_path: "/metrics"
node_exporter_textfile_dir: "/var/lib/node_exporter"
node_exporter_tls_server_config: {}
node_exporter_http_server_config: {}
node_exporter_basic_auth_users:
  user: password (bcrypt)
```  

### Конфигурация target в Prometheus

```
  - job_name: 'node_exporter'
## Блок определения mTLS  ##
    scheme: http(s)
#    tls_config:
#      ca_file: node_exporter.crt
    basic_auth:
      username: prometheus
      password: prometheus_redmine
    static_configs:
    - targets: ['dev:9100']
```
