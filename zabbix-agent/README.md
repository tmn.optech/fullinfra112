## Исполнение роли

Роль предназначена для установки zabbix-agent и авторегистрацией в Zabbix server.

Роль поддерживает установку и регистрации агента или через tag только перегистрацию,

Поддерживает OS: deb base, rpm base (set fact).

    

### example playbook
```
- hosts: all
  remote_user: panarinis
  gather_facts: yes
  become: true
  roles:
    - zabbix-agent
```

### Определяем переменную path к ролям или default /etc/ansible/roles/zabbix-agent

```
ansible-playbook -i inventory playbook.yml -vvv
```

### Кейс если zabbix агент установлен, требуется изменить описание хоста через zabbix api

```
ansible-playbook -i inventory playbook.yml --tags "api" -vvv
```

### Часто используемые переменные 

```
default/main.yml
### Определяем версию агента  и endpoint сервера
zabbix_agent_version: 4.0
zabbix_agent_server: monitoring.rm.mosreg.ru

### Определяем группу и template
zabbix_host_groups:
  - Linux servers
  - {{ other group - например Redmine}}
zabbix_link_templates:
  - Template OS Linux
  - {{ other template }}

### Креды к zabbix API  
zabbix_api_http_user: Admin
zabbix_api_http_password: xxxxxxx
zabbix_api_user: Admin
zabbix_api_pass: xxxxxxx
```

>  Примечание отоброжаемое имя агента берется из инвентори файла
пример:

```
devtest ansible_host=10.10.32.30
```
  

